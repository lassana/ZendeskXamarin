﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace ZendeskSDK.Droid.Example2
{
    [Activity(Label = "ZendeskSDK.Droid.Example", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            Button button = FindViewById<Button>(Resource.Id.myButton);
            button.Click += (sender, e) =>
            {
                var builder = Com.Zendesk.Belvedere.Belvedere.From(this);
                var belvedere = builder.Build();
            };
        }
    }
}

