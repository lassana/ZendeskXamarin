﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Android.Content;

namespace ZendeskSDK.Droid.Example
{
    [Activity(Label = "ZendeskSDK.Droid.Example", 
              MainLauncher = true, 
              Icon = "@mipmap/icon",
              Theme = "@style/MyTheme",
              LaunchMode = LaunchMode.SingleTop,
              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            Button button = FindViewById<Button>(Resource.Id.myButton);
            button.Click += (sender, e) =>
            {
                var builder = Com.Zendesk.Belvedere.Belvedere.From(this);
                var belvedere = builder.Build();
            };
        }
    }
}

